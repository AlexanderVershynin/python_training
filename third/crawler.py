import aiohttp
import asyncio
import re
import urllib.parse
import os
import threading

class Crawler:
    def __init__(self):
        self.queue = asyncio.Queue()
        self.url_set = set()

    async def get_links(self, html, base):
        base_url_parse_res = urllib.parse.urlparse(base)
        base_scheme = base_url_parse_res.scheme
        base_loc = base_url_parse_res.netloc
        if not base_scheme:
            base_scheme = 'http'
        links = re.findall('''href=[\'"]?([^\'" >]+)''', html)
        for i, link in enumerate(links):
            url_parse_res = urllib.parse.urlparse(link)
            if url_parse_res.netloc:
                #print(f"!!!{link}")
                continue
            elif link.startswith('..'):
                #external url
                #print(f"!!!{link}")
                continue

            #Question this doe not work:
            #returns: base www.karpaty-slav.com link vytyag-slavsko.html link final vytyag-slavsko.html
            #link_with_base = urllib.parse.urljoin(urllib.parse.urlparse(base).netloc, link)
            link_with_base = base_scheme+'://'+base_loc+'/'+link
            #print(f"base {urllib.parse.urlparse(base).netloc} link {link} link final {link_with_base}")

            if link_with_base not in self.url_set:
                self.url_set.add(link_with_base)
                self.queue.put_nowait(link_with_base)


    async def fetch(self, client):
        url = await self.queue.get()
        print(f'url: {url} thread id: {threading.get_ident()}')
        async with client.get(url) as resp:
            if not resp.status == 200:
                self.queue.task_done()
                return {'base': None,
                    'html': None
                }
            parsed = urllib.parse.urlparse(url)
            base = f"{parsed.scheme}://{parsed.netloc}"
            resp_text = await resp.text()
            self.queue.task_done()
            return {'base': base,
                'html': resp_text
            }


    async def process(self):
        sem = asyncio.Semaphore(10)
        self.queue.put_nowait('http://www.karpaty-slav.com')
        while not self.queue.empty():
            async with sem:
                async with aiohttp.ClientSession() as session:
                    print(f'process, thread id = {threading.get_ident()}')
                    res = await self.fetch(session)
                    html = res['html']
                    link = res['base']
                    if html == None:
                        continue
                    await self.get_links(html, link)
                    #print(html)


if __name__ == '__main__':
    crawler = Crawler()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(crawler.process())
