#"2. Working with Python Pandas
#For this exercise, please download the Automobile Dataset from https://pynative.com/wp-content/uploads/2019/01/Automobile_data.csv
#
#Tasks:
#- From given data set print first and last five rows
#- Replace all column values which contain ‘?’ and n.a with NaN.
#- Print most expensive car’s company name and price.
#- Print All Toyota Cars details
#- Count total cars per company
#- Find each company’s Highest price car
#- Find the average mileage of each car making company
#- Sort all cars by Price column
#- Create two data frames using the following two Dicts, Concatenate those two data frames and create a key for each data frame:
#GermanCars = {'Company': ['Ford', 'Mercedes', 'BMV', 'Audi'], 'Price': [23845, 171995, 135925 , 71400]}
#japaneseCars = {'Company': ['Toyota', 'Honda', 'Nissan', 'Mitsubishi '], 'Price': [29995, 23600, 61500 , 58900]}
#- Create two data frames using the following two Dicts, Merge two data frames, and append second data frame as a new column to the first data frame.
#Car_Price = {'Company': ['Toyota', 'Honda', 'BMV', 'Audi'], 'Price': [23845, 17995, 135925 , 71400]}
#car_Horsepower = {'Company': ['Toyota', 'Honda', 'BMV', 'Audi'], 'horsepower': [141, 80, 182 , 160]}"

import pandas as pd


class Database:
    def __init__(self):
        self.data_frame = pd.read_csv('./Automobile_data.csv', index_col=0, engine='c', encoding = "utf-8")

    def output_first_five(self):
        print('First file series:')
        print(self.data_frame.head())

    def output_last_five(self):
        print('Last file series:')
        print(self.data_frame.tail())

    def inputation(self):
        print('Inputation...')
        null_sum = self.data_frame.isnull().sum()
        nul_columns = [ col for col in self.data_frame if null_sum[col] > 0 ]
        print(nul_columns)
        for col_name in nul_columns:
            column = self.data_frame[col_name]
            #it would be better to replace here with some average value
            column.fillna(float('nan'), inplace=True)
        print(self.data_frame.isnull().sum())

    def out_most_expansive(self):
        print("Searching most expansive company...")
        #https://stackoverflow.com/questions/15741759/find-maximum-value-of-a-column-and-return-the-corresponding-row-values-using-pan
        exp_rec = self.data_frame.loc[self.data_frame['price'].idxmax(axis = 1, skipna = True)]
        print(f"Most expansive car company is {exp_rec['company']} with price equal to {exp_rec['price']}")

    def out_cars_by_company(self, company):
        print(f"cars produced by {company} company:")
        out = self.data_frame[self.data_frame['company'] == company]
        print(out)

    def total_cars_by_company(self):
        print('Cars count by company:')
        #https://www.shanelynn.ie/summarising-aggregation-and-grouping-data-in-python-pandas/
        out = self.data_frame.groupby('company')['company'].count()
        print(out)

    def highest_price_car_by_company(self):
        print('Highest price car by company:')
        out = self.data_frame.loc[self.data_frame.groupby('company')['price'].idxmax(axis = 1, skipna = True)]
        print(out)

    def average_mileage(self):
        print('Average mileage by company...')
        out = out = self.data_frame.groupby('company')['average-mileage'].mean()
        print(out)

    def out_sorted(self):
        print('Sorted data frame by price:')
        #https://datatofish.com/sort-pandas-dataframe/
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #https://stackoverflow.com/questions/19124601/pretty-print-an-entire-pandas-series-dataframe
            print(self.data_frame.sort_values(by=['price']))


def concatenate_data_frames():
    #https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
    #QUESTION:  How to make index to be 0, 1, 2, 3, 4, 5, 6, 7
    #instead of 0, 1, 2, 3, 0, 1, 2, 3?
    german_cars = {'Company': ['Ford', 'Mercedes', 'BMV', 'Audi'], 'Price': [23845, 171995, 135925 , 71400]}
    japanese_cars = {'Company': ['Toyota', 'Honda', 'Nissan', 'Mitsubishi '], 'Price': [29995, 23600, 61500 , 58900]}
    df1 = pd.DataFrame(german_cars, columns= ['Company', 'Price'])
    df2 = pd.DataFrame(japanese_cars, columns= ['Company', 'Price'])
    result = pd.concat([df1, df2], keys=['german', 'japanise'])
    print('concatenation result:')
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(result)

def merge_data_frames():
    car_price = {'Company': ['Toyota', 'Honda', 'BMV', 'Audi'], 'Price': [23845, 17995, 135925 , 71400]}
    car_horsepower = {'Company': ['Toyota', 'Honda', 'BMV', 'Audi'], 'horsepower': [141, 80, 182 , 160]}
    df1 = pd.DataFrame(car_price, columns= ['Company', 'Price'])
    df2 = pd.DataFrame(car_horsepower, columns= ['Company', 'horsepower'])
    #https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.set_index.html
    #QUESTION: why index is not set, I see 1, 2, 3 in output
    df1.set_index('Company')
    df2.set_index('Company')
    result = pd.merge(df1, df2, on='Company')
    result.set_index('Company')
    #QUESTION: why index is not set, I see 1, 2, 3 in output
    print('merging result:')
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(result)



if __name__ == '__main__':
    db = Database()
    db.output_first_five()
    db.output_last_five()
    db.inputation()
    db.out_most_expansive()
    db.out_cars_by_company('toyota')
    db.total_cars_by_company()
    db.highest_price_car_by_company()
    db.average_mileage()
    db.out_sorted()
    concatenate_data_frames()
    merge_data_frames()
