class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]

class Logger(metaclass=Singleton):
    def __init__(self):
        print('Init of logger is called')

    def log(self, txt):
        print(f'{txt} instance: {self}')

if __name__=='__main__':
    log = Logger()
    log.log('Hi')

    log1 = Logger()
    log1.log('Sasha')
