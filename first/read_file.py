class TextReader:
    def __init__(self):
        self.file_name = ''

    def input_file_name(self):
        self.file_name = str(input('please input the file name:'))
        pass

    def user_control(self):
        s = str(input('press Enter to read the next line and n+Enter to stop reading the file'))
        if s == 'n':
            return False
        if s == 'q':
            quit()
        return True;

    def process_file(self):
        while(True):
            self.input_file_name()
            f = open(self.file_name, "r")
            flines = f.readlines()
            ln = 0
            for line in flines:
                ln = ln + 1
                print(f'{ln} {line}')
                if not self.user_control():
                    break;

if __name__=='__main__':
    reader = TextReader()
    reader.process_file()
