from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy import ForeignKey
from sqlalchemy import text

from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship

import os
import lxml.etree
from datetime import datetime


Base = declarative_base()

class Hospital(Base):
    __tablename__ = 'Hospital'

    Hospital_Id = Column(Integer, primary_key=True)
    Hospital_Name = Column(String)
    Bed_Count = Column(Integer)

    def __repr__(self):
        return "<Hospital(id='%d', name='%s', beds='%d')>" % (
            self.Hospital_Id, self.Hospital_Name, self.Bed_Count)


class Doctor(Base):
    __tablename__ = 'Doctor'
    Doctor_Id = Column(Integer, primary_key=True)
    Doctor_Name = Column(String, nullable=False)
    Hospital_Id = Column(Integer, ForeignKey('Hospital.Hospital_Id'), nullable=False)
    hospital = relationship("Hospital", back_populates="doctors")
    Joining_Date = Column(Date, nullable=False)
    Speciality = Column(String, nullable=True)
    Salary = Column(Integer, nullable=True)
    Experience = Column(Integer, nullable=True)


    def __repr__(self):
        return f"<Doctor(id={self.Doctor_Id}, name={self.Doctor_Name}, hospital_id={self.Hospital_Id}," \
            f"joining date={self.Joining_Date}, speciality={self.Speciality}, salary={self.Salary}," \
            f"experience={self.Experience})>"


Hospital.doctors = relationship("Doctor", order_by=Doctor.Doctor_Id, back_populates="hospital")


class Database:
    def __init__(self):
        self.db_path = '/Users/alex/projects/python_learning/second_db/db.db'
        self.__unlink()
        self.__create_fill_database()

    def __create_fill_database(self):
        self.engine = create_engine(f'sqlite:///{self.db_path}', echo=False)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        self.session.add_all([
            Hospital(Hospital_Id=1, Hospital_Name='Mayo Clinic', Bed_Count=200),
            Hospital(Hospital_Id=2, Hospital_Name='Cleveland Clinic', Bed_Count=400),
            Hospital(Hospital_Id=3, Hospital_Name='Johns Hopkins', Bed_Count=1000),
            Hospital(Hospital_Id=4, Hospital_Name='UCLA Medical Center', Bed_Count=1500)
        ])

        self.session.commit()

        query = self.session.query(Hospital).filter(text('Hospital.Hospital_Id=:id'))

        cur_hospital = query.params(id=1).one()
        cur_hospital.doctors = [
            Doctor(Doctor_Id=101, Doctor_Name='David', Joining_Date=datetime.strptime('2005-02-10', '%Y-%m-%d'), Speciality='Pediatric', Salary=40000),
            Doctor(Doctor_Id=102, Doctor_Name='Michael', Joining_Date=datetime.strptime('2018-07-23', '%Y-%m-%d'), Speciality='Oncologist', Salary=20000)
        ]

        cur_hospital = query.params(id=2).one()
        cur_hospital.doctors = [
            Doctor(Doctor_Id=103, Doctor_Name='Susan', Joining_Date=datetime.strptime('2016-05-19', '%Y-%m-%d'), Speciality='Garnacologist', Salary=25000),
            Doctor(Doctor_Id=104, Doctor_Name='Robert', Joining_Date=datetime.strptime('2017-12-28', '%Y-%m-%d'), Speciality='Pediatric', Salary=28000)
        ]

        cur_hospital = query.params(id=3).one()
        cur_hospital.doctors = [
            Doctor(Doctor_Id=105, Doctor_Name='Linda', Joining_Date=datetime.strptime('2004-06-04', '%Y-%m-%d'), Speciality='Garnacologist', Salary=42000),
            Doctor(Doctor_Id=106, Doctor_Name='William', Joining_Date=datetime.strptime('2012-09-11', '%Y-%m-%d'), Speciality='Dermatologist', Salary=30000)
        ]

        cur_hospital = query.params(id=4).one()
        cur_hospital.doctors = [
            Doctor(Doctor_Id=107, Doctor_Name='Richard', Joining_Date=datetime.strptime('2014-08-21', '%Y-%m-%d'), Speciality='Garnacologist', Salary=32000),
            Doctor(Doctor_Id=108, Doctor_Name='Karen', Joining_Date=datetime.strptime('2011-10-17', '%Y-%m-%d'), Speciality='Radiologist', Salary=30000)
        ]
        self.session.commit()

    def __unlink(self):
        if os.path.exists(self.db_path):
            os.remove(self.db_path)

    def print_db(self):
        for hosp in self.session.query(Hospital).order_by(Hospital.Hospital_Id).all():
            print()
            print(hosp)
            print("It's doctors:")
            for doc in hosp.doctors:
                print(doc)

    def get_doctors_by_speciality(self, doctor_speciality):
        for doc in self.session.query(Doctor).filter(Doctor.Speciality.like(f'{doctor_speciality}%')).order_by(Doctor.Experience, Doctor.Salary).all():
            print(doc)

    def update_doctor_experience(self, doctor_id, experience):
        doctor = self.session.query(Doctor).filter(Doctor.Doctor_Id == doctor_id).one()
        doctor.Experience = experience
        self.session.commit()
        print(doctor)

    def print_to_xml(self):
        doctors_root = lxml.etree.Element('doctors')
        for doc_rec in self.session.query(Doctor).order_by(Doctor.Experience, Doctor.Salary).all():
            doctor = lxml.etree.SubElement(doctors_root, 'Doctor')
            doctor_id = lxml.etree.SubElement(doctor, 'Doctor_id')
            doctor_id.text = str(doc_rec.Doctor_Id)
            personal_data = lxml.etree.SubElement(doctor, 'PersonalData')
            doc_name = lxml.etree.SubElement(personal_data, 'name')
            doc_name.text = doc_rec.Doctor_Name
            doc_speciality = lxml.etree.SubElement(personal_data, 'speciality')
            doc_speciality.text = doc_rec.Speciality
            doc_salary = lxml.etree.SubElement(personal_data, 'salary')
            doc_salary.text = str(doc_rec.Salary)
        print(lxml.etree.tounicode(doctors_root, pretty_print=True))

if __name__=='__main__':
    db = Database()
    db.print_db()
    s = str(input('Please input the speciality and I will output apropriate doctors\n'))
    db.get_doctors_by_speciality(s)
    doc_id = str(input('Please input the doctor id for which you will update Experience\n'))
    experience = int(input('Please enter Experience\n'))
    db.update_doctor_experience(doc_id, experience)
    db.print_to_xml()
